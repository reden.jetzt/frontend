import { DefaultService } from './../_api/api/default.service';
import { TopicDto } from './../_api/model/topicDto';
import {Injectable} from '@angular/core';
import {Topic} from '../models/topic';

declare var Fuse: any;

@Injectable({
    providedIn: 'root'
})
export class TopicService {

    constructor(
      private defaultService: DefaultService
    ) {
    }

    fuzzySearchTopics(topics: Topic[], searchSearchTerm: string, availableSlots: number): Topic[] {
        const options = {
            shouldSort: true,
            threshold: 0.6,
            location: 0,
            distance: 100,
            minMatchCharLength: 1,
            keys: [
                'name',
                'tags'
            ]
        };
        const fuse = new Fuse(topics, options); // "list" is the item array

        // map has to be done, because fuse return the items under a "item" property each
        const result: Array<Topic> = fuse.search(searchSearchTerm).slice(0, availableSlots).map((item: any) => item.item);
        if (result.length > 0) {
            return result;
        } else {
            return topics.sort((a: Topic, b: Topic) => {
                const aScore = a.peopleInQueue * 5 + a.peopleInTotal;
                const bScore = b.peopleInQueue * 5 + b.peopleInTotal;
                return aScore > bScore ? -1 : 1;
            });
        }
    }

    async getCurrentTopics(): Promise<Topic[]> {
        return new Promise((resolve, reject) => {
        this.defaultService.topicsControllerFindAll().subscribe((result: TopicDto[]) => {
            resolve(result as Topic[]);
        });
      });
    }

    async uuidsToTopics(uuids: string[]): Promise<Topic[]> {
      const topics = await this.getCurrentTopics();
      return uuids.map<Topic>((uuid: string) => {
        return topics.find((topic: Topic) => {
          return topic.id === uuid;
        });
      });


    }

    async getMatchingTopics(uuids1: string[], uuids2: string[]): Promise<Topic[]> {
      const filtered = uuids1.filter((uuidOfOne) => {
        return uuids2.includes(uuidOfOne);
      });

      return this.uuidsToTopics(filtered);
    }
}
