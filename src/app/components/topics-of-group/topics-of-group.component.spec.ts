import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicsOfGroupComponent } from './topics-of-group.component';

describe('TopicsOfGroupComponent', () => {
  let component: TopicsOfGroupComponent;
  let fixture: ComponentFixture<TopicsOfGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopicsOfGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicsOfGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
