import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineCounterComponent } from './online-counter.component';

describe('OnlineCounterComponent', () => {
  let component: OnlineCounterComponent;
  let fixture: ComponentFixture<OnlineCounterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlineCounterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
