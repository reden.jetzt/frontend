import {Topic} from './topic';

export interface Person {
    name: string;
    topics: Topic[];
}
