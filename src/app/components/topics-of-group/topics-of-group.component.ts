import {Component, Input, OnInit} from '@angular/core';
import {Person} from '../../models/person';
import {Topic} from '../../models/topic';

@Component({
    selector: 'app-topics-of-group',
    templateUrl: './topics-of-group.component.html',
    styleUrls: ['./topics-of-group.component.css']
})
export class TopicsOfGroupComponent implements OnInit {
    @Input() participants: Person[] = [];

    get topics(): Array<Topic> {
        const temp: Array<Topic> = [];
        this.participants.forEach((participant: Person) => {
            participant.topics.forEach((topic: Topic) => {
                if (!temp.map((value: Topic) => value.id).includes(topic.id)) {
                    temp.push(topic);
                }
            });
        });
        return temp;
    }

    constructor() {
    }

    ngOnInit(): void {
    }

}
