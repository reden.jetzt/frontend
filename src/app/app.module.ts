import { environment } from './../environments/environment';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HomeComponent} from './pages/home/home.component';
import {QueueComponent} from './pages/queue/queue.component';
import {MatchFoundComponent} from './pages/match-found/match-found.component';
import {CallComponent} from './pages/call/call.component';
import {Routes, RouterModule} from '@angular/router';
import {TopicSelectorComponent} from './components/topic-selector/topic-selector.component';
import {AgeSelectorComponent} from './components/age-selector/age-selector.component';
import {OnlineCounterComponent} from './components/online-counter/online-counter.component';
import {PartnerInformationComponent} from './components/partner-information/partner-information.component';
import {ChatComponent} from './components/chat/chat.component';
import {TopicsOfGroupComponent} from './components/topics-of-group/topics-of-group.component';
import {MuteButtonComponent} from './components/mute-button/mute-button.component';
import {ApiModule, Configuration} from './_api';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {TopicSelectorButtonComponent} from './components/topic-selector-button/topic-selector-button.component';
import {Ng5SliderModule} from 'ng5-slider';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { NumericDirective } from './directives/nummeric-directive.directive';
import { ImprintComponent } from './pages/imprint/imprint.component';
import { PrivacyComponent } from './pages/privacy/privacy.component';

const config: SocketIoConfig = { url: environment.socketIOUrl, options: {
  path: ''
 }
};


export function getApiConfig(): Configuration {
    return new Configuration({
        basePath: environment.apiBasePath,
        apiKeys: {}
    });
}


const appRoutes: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'match-found', component: MatchFoundComponent},
    {path: 'queue', component: QueueComponent},
    {path: 'call/:id', component: CallComponent},
    {path: 'call', component: CallComponent},
    {path: 'imprint', component: ImprintComponent},
    {path: 'privacy', component: PrivacyComponent},
    {path: '', component: HomeComponent}
];

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        QueueComponent,
        MatchFoundComponent,
        CallComponent,
        TopicSelectorComponent,
        AgeSelectorComponent,
        OnlineCounterComponent,
        PartnerInformationComponent,
        ChatComponent,
        TopicsOfGroupComponent,
        MuteButtonComponent,
        TopicSelectorButtonComponent,
        NumericDirective,
        ImprintComponent,
        PrivacyComponent,
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(
            appRoutes,
            {enableTracing: true}
        ),
        Ng5SliderModule,
        ApiModule.forRoot(getApiConfig),
        HttpClientModule,
        FormsModule,
        SocketIoModule.forRoot(config),
        SweetAlert2Module.forRoot()
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
