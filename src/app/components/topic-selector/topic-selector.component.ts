import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {DefaultService, TopicDto} from '../../_api';
import {Topic} from '../../models/topic';
import {TopicService} from '../../services/topic.service';

@Component({
    selector: 'app-topic-selector',
    templateUrl: './topic-selector.component.html',
    styleUrls: ['./topic-selector.component.scss']
})
export class TopicSelectorComponent implements OnInit {
    public searchTerm = '';
    public selectableTopics: Topic[] = [];
    public selectedTopics: Topic[] = [];

    @Output() selectedTopicsOutput = new EventEmitter<Topic[]>();

    public filteredTopics(): Topic[] {
        if (this.selectableTopics !== undefined) {
            const toSearch: Topic[] = this.selectableTopics;
            return this.topicService.fuzzySearchTopics(
                this.selectableTopics.filter((value: Topic) => {
                    return !this.selectedTopics.includes(value);
                }),
                this.searchTerm,
                6
            );
        } else {
            return [];
        }
    }

    constructor(
      private defaultService: DefaultService,
      private topicService: TopicService) {
    }

    ngOnInit(): void {
        this.getCurrentTopics();
    }

    getCurrentTopics(): void {
        this.defaultService.topicsControllerFindAll().subscribe((result: TopicDto[]) => {
            this.selectableTopics = result as Topic[];
        });
    }

    selectionStateChanged(topic: Topic) {
        if (!this.selectedTopics.includes(topic)) {
            this.selectedTopics.push(topic);
        } else {
            this.selectedTopics.splice(this.selectedTopics.indexOf(topic), 1);
        }
        this.selectedTopicsOutput.emit(this.selectedTopics);
    }

    getTopicForButton(index: number): Topic {
        const concatenated = this.selectedTopics.concat(this.filteredTopics());
        if (index >= concatenated.length) {
            return null;
        } else {
            return concatenated[index];
        }
    }
}
