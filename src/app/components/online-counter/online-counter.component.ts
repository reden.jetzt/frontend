import { Component, AfterViewInit, ViewChild, ElementRef, Input, OnChanges, SimpleChanges, Optional} from '@angular/core';

@Component({
  selector: 'app-online-counter',
  templateUrl: './online-counter.component.html',
  styleUrls: ['./online-counter.component.scss']
})
export class OnlineCounterComponent implements OnChanges, AfterViewInit {
  @Input() digit: number;
  @Optional() @Input() duration: number;
  @Optional() @Input() steps: number;
  @ViewChild('animatedDigit') animatedDigit: ElementRef;

  constructor(private elRef: ElementRef) {
    this.animatedDigit = elRef;
  }

  animateCount() {
    if (!this.duration) {
      this.duration = 1000;
    }

    this.counterFunc(this.digit, this.duration, this.animatedDigit);
  }

  counterFunc(endValue: number, durationMs: number, element: any) {
    if (!this.steps) {
      this.steps = 12;
    }

    const stepCount = Math.abs(durationMs / this.steps);
    const valueIncrement = (endValue - 0) / stepCount;
    const sinValueIncrement = Math.PI / stepCount;

    let currentValue = 0;
    let currentSinValue = 0;

    function step() {
      currentSinValue += sinValueIncrement;
      currentValue += valueIncrement * Math.sin(currentSinValue) ** 2 * 2;

      element.nativeElement.textContent = Math.abs(Math.floor(currentValue));

      if (currentSinValue < Math.PI) {
        window.requestAnimationFrame(step);
      }
    }

    step();
  }

  ngAfterViewInit() {
    if (this.digit) {
      this.animateCount();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['digit']) {
      this.animateCount();
    }
  }

}
