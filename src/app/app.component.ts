import { CallService } from './services/call.service';
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'reden-jetzt';

  constructor(
    private callService: CallService
  ) { }

  ngOnInit(): void {
    this.callService.initPeer();
  }

}
