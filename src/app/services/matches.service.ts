import { MatchFoundResponse } from './../models/matchFoundResponse';
import { RandomConnectDto } from './../models/dto/randomConnect.dto';
import { ChangeNameDto } from './../models/dto/changeName.dto';
import { SignupDto } from './../models/dto/signup.dto';
import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class MatchesService {

  constructor(
    private socket: Socket
  ) { }

  public socketInit() {
  }

  public signUp(signUpDto: SignupDto) {
    this.socket.emit(
      'signup', signUpDto
    );
  }
  public changeName(changeNameDto: ChangeNameDto) {
    this.socket.emit(
      'changename', changeNameDto
    );
  }

  public randomConnect() {
    this.socket.emit(
      'randomconnect'
    );
  }

  public findMatch() {
    this.socket.emit(
      'findmatch'
    );
  }

  public OnMatchFound(cb: (resp: MatchFoundResponse) => void ) {
    this.socket.on('foundmatch', (data) => {
      cb(data as MatchFoundResponse);
    });
  }

  public OnTotalConnections(cb: (connection: number) => void) {
    this.socket.on('totalconnections', (data: TotalConnections) => {
      cb(data.totalConnections);
    });
  }
}

interface TotalConnections {
  totalConnections: number;
}
